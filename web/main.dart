import 'dart:html';
import 'dart:math';

void main() {
  var result = querySelector('#theResult');
  var die = Random();

  querySelector('#dtwo').onClick.listen((e) {
    var newresult = die.nextInt(2);
    switch (newresult){
    case 0:
      result.text = "It's tails.";
      break;
    case 1:
      result.text = "It's heads.";
      break;
    }
  });

  querySelector('#dfour').onClick.listen((e) {
    var newresult = die.nextInt(4) + 1;
    result.text = 'Your tiny caltrop rolled a $newresult.';
  });

  querySelector('#dsix').onClick.listen((e) {
    var newresult = die.nextInt(6) + 1;
    result.text = 'The ubiquitous d6. Yours probably came from a Monopoly set, right? Anyways, it landed on $newresult.';
  });

  querySelector('#deight').onClick.listen((e) {
    var newresult = die.nextInt(8) + 1;
    result.text = 'If the d4 was bad enough... You got a $newresult.';
  });

  querySelector('#dten').onClick.listen((e) {
    var newresult = die.nextInt(10) + 1;
    result.text = 'Ah, the d10. One half the average set used for percentiles. It is a $newresult.';
  });

  querySelector('#dtwelve').onClick.listen((e) {
    var newresult = die.nextInt(12) + 1;
    result.text = 'The d12 is an odd shape. It rolled a $newresult.';
  });

  querySelector('#dtwenty').onClick.listen((e) {
    var newresult = die.nextInt(20) + 1;
    result.text = 'All hail the mighty d20! You rolled a $newresult.';
  });

  querySelector('#adv-disadv').onClick.listen((e) {
    var d20roll1 = die.nextInt(20) + 1;
    var d20roll2 = die.nextInt(20) + 1;
    result.text = 'You rolled for either advantage or disadvantage. The results are $d20roll1 and $d20roll2.';
  });

  querySelector('#percentiles').onClick.listen((e) {
    var percents = die.nextInt(100) + 1;
    result.text = 'You rolled $percents%.';
  });
}
