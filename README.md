A dice roller application written in dart.

To compile the code, install the Dart SDK and either -

1. run ````webdev serve````
2. change into the web directory and run ````dart2js main.dart -o main.dart.js -m````

Created from templates made available by Stagehand under a BSD-style
[license](https://github.com/dart-lang/stagehand/blob/master/LICENSE).
